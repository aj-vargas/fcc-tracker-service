'use strict';

const moment = require('moment');
const ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;

const ERROR = {
  USER_INEXISTENT: {
    ID: Symbol('ERROR.USER_INEXISTENT'),
    MESSAGE: 'user doesn\'t exist'
  }
};

const mongo = {
  client: null,
  db: null,
  collection: null
};

async function connect() {
  mongo.client = await MongoClient.connect(
    process.env.MONGO_URI,
    {useNewUrlParser: true}
  );
  mongo.db = mongo.client.db();
  mongo.collection = mongo.db.collection('exercise');
}

async function disconnect() {
  await mongo.client.close();
}

async function createUser(name) {
  const result = await mongo.collection.insertOne({name});
  return result.ops[0]._id.toString();
}

async function createExercise(id, description, duration, at) {
  const atMoment = moment.utc(at || undefined);
  const user = await mongo.collection.findOne({_id: new ObjectId(id)});
  if (!user) {
    throw ERROR.USER_INEXISTENT;
  }
  await mongo.collection.updateOne(
    {_id: new ObjectId(id)},
    {$push: {log: {description, duration, at: atMoment.toDate()}}}
  );
  return {
    id,
    name: user.name,
    description,
    duration,
    at: atMoment.toISOString()
  };
}

async function readExercise(id, from, to, limit = 10) {
  const fromMoment = moment.utc(from || undefined).startOf('day');
  const toMoment = moment.utc(to || undefined).endOf('day');
  const result = await mongo.collection
    .aggregate([
      {$match: {_id: new ObjectId(id)}},
      {$project: {
        name: true,
        log: {
          $filter: {
            input: '$log',
            cond: {
              $and: [
                {$gte: ['$$this.at', fromMoment.toDate()]},
                {$lt: ['$$this.at', toMoment.toDate()]}
              ]
            }
          }
        }
      }}
    ])
    .toArray();
  result[0].id = result[0]._id.toString();
  delete result[0]._id;
  result[0].log.sort((a, b) => moment.utc(b.at).diff(moment.utc(a.at)));
  result[0].log.splice(limit);
  result[0].count = result[0].log.length;
  result[0].log = result[0].log.map(i => ({
    description: i.description,
    duration: i.duration,
    at: i.at.toISOString()
  }));
  return result[0];
}

exports.ERROR = ERROR;
exports.connect = connect;
exports.disconnect = disconnect;
exports.createUser = createUser;
exports.createExercise = createExercise;
exports.readExercise = readExercise;
