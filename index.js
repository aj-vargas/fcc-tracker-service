'use strict';
require('dotenv').config();

const express = require('express');
const dao = require('./dao');

const app = express();
let server = null;

app.use(express.static('public'));
app.use(express.urlencoded({extended: true}));

app.get('/api/exercise/log', async (request, response) => {
  if (!request.query.id) {
    return response.json({error: 'user id must be provided'});
  }
  try {
    const log = await dao.readExercise(
      request.query.id,
      request.query.from,
      request.query.to,
      request.query.limit || undefined
    );
    if (log) {
      return response.json(log);
    }
    return response.json({error: 'couldn\'t load exercise log'});
  } catch (error) {
    return response.json({error: 'couldn\'t load exercise log'});
  }
});

app.post('/api/exercise/new-user', async (request, response) => {
  if (!request.body.name) {
    return response.json({error: 'user name must be provided'});
  }
  try {
    const id = await dao.createUser(request.body.name);
    return response.json({id, name: request.body.name});
  } catch (error) {
    if (error.code === 11000) {
      return response.json({error: 'user name already exists'});
    }
    return response.json({error: 'couldn\'t create user'});
  }
});

app.post('/api/exercise/new-entry', async (request, response) => {
  const error = [];
  if (!request.body.id) {
    error.push('user id must be provided');
  }
  if (!request.body.description) {
    error.push('description must be provided');
  }
  if (!/^\d{1,}$/.test(request.body.duration)) {
    error.push('duration must be provided and be a number');
  }
  if (error.length > 0) {
    return response.json({error});
  }
  try {
    const entry = await dao.createExercise(
      request.body.id,
      request.body.description,
      request.body.duration,
      request.body.at
    );
    return response.json(entry);
  } catch (error) {
    if (error.ID === dao.ERROR.USER_INEXISTENT.ID) {
      return response.json({error: dao.ERROR.USER_INEXISTENT.MESSAGE});
    }
    return response.json({error: 'couldn\'t create log entry'});
  }
});

async function clean() {
  await Promise.all([
    new Promise(resolve => server.close(resolve)),
    dao.disconnect()
  ]);
}

async function main() {
  await dao.connect();
  server = app.listen(8080);
}

process.on('SIGINT', clean);
process.on('SIGTERM', clean);

main();
